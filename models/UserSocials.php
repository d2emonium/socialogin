<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $user_id
 * @property string $user_social_vk
 * @property string $user_social_ok
 * @property string $user_social_fb
 *
 * The followings are the available model relations:
 * @property Cities $city
 * @property UserPhones $user
 */
class UserSocials extends CActiveRecord
{
    public $user_id = 0;
    public $user_social_vk = '';
    public $user_social_ok = '';
    public $user_social_fb = '';
    public $social_data = '';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_socials';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			['user_id', 'required'],
			['user_id', 'numerical', 'integerOnly'=>true],
			['user_social_vk, user_social_ok, user_social_fb', 'length', 'max'=>32], /**/
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
        ];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
