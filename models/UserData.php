<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $user_id
 * @property string $user_social_vk
 * @property string $user_social_ok
 * @property string $user_social_fb
 *
 * The followings are the available model relations:
 * @property Cities $city
 * @property UserPhones $user
 */
class UserData extends CModel
{
    public $user_id = 0;
    public $user_firstname = '';
    public $user_lastname  = '';
    public $user_bdate     = '';
    public $user_city      = '';
    public $user_photo     = '';
    public $user_phone     = '';

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            ['user_id, user_firstname, user_lastname, user_bdate, user_city, user_photo, user_phone', 'safe'], /**/
        );
    }

    /**
     * [response] => Array (
     *      [0] => Array (
     *          [id] => 3987598
     *          [first_name] => Дмитрий
     *          [last_name] => Куценко
     *          [bdate] => 22.12.1985
     *          [city] => Array (
     *              [id] => 552
     *              [title] => Луганск
     *          )
     *          [country] => Array (
     *              [id] => 2
     *              [title] => Украина
     *          )
     *          [photo_max] => http://cs617730.vk.me/v617730598/e8fd/tW-Rd7tUhtw.jpg
     *          [home_phone] => Устарел
     *      )
     * )
     */

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function attributeNames()
    {
        return [
            'user_id',
            'user_firstname',
            'user_lastname',
            'user_bdate',
            'user_city',
            'user_photo',
            'user_phone',

        ];
    }
}
