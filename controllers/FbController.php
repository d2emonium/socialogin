<?php

class FbController extends SocialController
{
    var $scheme          = "http://";
    var $social_url      = "facebook.com";

    var $login_url       = 'dialog/oauth';
    var $login_subdomain = "www";

    public function getClient_id()
    {
        return $this->module->fb['client_id'];
    }

    public function getClient_secret()
    {
        return $this->module->fb['client_secret'];
    }

    public function getResponse_type()
    {
        return $this->module->fb['response_type'];
    }

    public function actionLogin()
    {
        // Анализируем строку на наличие code.
        if(isset($_GET['code']) && $_GET['code'])
        {
            $code = $_GET['code'];

            /*
            $url = $this->generateUrl('oauth/access_token', [
                "client_id"     => $this->client_id,
                "redirect_uri"  => $this->createAbsoluteUrl('ok/login'),
                "client_secret" => $this->client_secret,
                "code"          => $this->code,
            ], "graph");
            $data = $this->getFromUrl($url);
            */
            $url = $this->generateUrl('oauth/access_token', [], "graph");
            $data = $this->getFromUrl($url, "get", [
                "client_id"     => $this->client_id,
                "redirect_uri"  => $this->createAbsoluteUrl('login'),
                "client_secret" => $this->client_secret,
                "code"          => $code,
            ]);
            $param_list = explode('&', $data);
            print_r($param_list);
            $params = [];
            /*foreach($param_list as $param)
            {
                $param_data = explode('=', $param);
                $params[] = [
                    $param_data[0] => $param_data[1],
                ];
            }*/

            $access_token = isset($params['access_token']) ? $params['access_token'] : 1;
            /*
            $url = $this->generateUrl('me', [
                "access_token"    => $access_token,
                "application_key" => $this->public_key,
            ], "graph");
            $data = $this->getFromUrl($url);
            $params = json_decode($data);
            */
            $url = $this->generateUrl('me', [], "graph");
            $data = $this->getFromUrl($url, "get", [
                "access_token"    => $access_token,
                "application_key" => $this->public_key,
            ]);

            print_r($params);
        }
        else
        {
            //http://mysite.com/vklogin?error=invalid_request&error_description=Invalid+display+parameter
        }
    }
}