<?php

class VkController extends SocialController
{
    var $scheme          = "https://";
    var $social_url      = "vk.com";

    var $login_url       = 'authorize';
    var $login_subdomain = "oauth";

    var $auth_params = [
        'scope' => 'email',
        'v'     => "5.28",
    ];

    public function getClient_id()
    {
        return $this->module->vk['client_id'];
    }

    public function getClient_secret()
    {
        return $this->module->vk['client_secret'];
    }

    public function getResponse_type()
    {
        return $this->module->vk['response_type'];
    }

    public function actionLogin()
    {
        if(isset($_GET['code']) && $_GET['code'])
        {
            $code = $_GET['code'];

            $g = new SocialUrlGenerator();

            /*
            $url = $this->generateUrl('access_token', [
                "client_id"     => $this->client_id,
                "client_secret" => $this->client_secret,
                "code"          => $code,
            ], $this->login_subdomain);
            $data = $this->getFromUrl($url);
            */

            $url = $this->generateUrl('access_token', [], $this->login_subdomain);
            $data = $this->getFromUrl($url, "get", [
                "client_id"     => $this->client_id,
                "client_secret" => $this->client_secret,
                "redirect_url"  => $g->vk_url,
                "code"          => $code,

            ]);
            $access_params = CJSON::decode($data);

            $user_id      = isset($access_params['user_id'])      ? $access_params['user_id']      : 0;
            $access_token = isset($access_params['access_token']) ? $access_params['access_token'] : 0;
            $url = $this->generateUrl('method/users.get', [], "api");
            $data = $this->getFromUrl($url, "get", [
                "user_id"      => $user_id,
                "access_token" => $access_token,
                "fields"       => $this->module->vk['fields'],
            ]);
            $params = CJSON::decode($data);

            $social = UserSocials::model()->findByAttributes(['user_social_vk' => $user_id]);
            if(!$social)
            {
                $social = new UserSocials();
                $social->user_social_vk = $user_id;
                Yii::app()->session['user_social'] = $social->attributes;

                $response = isset($params['response'][0]) ? $params['response'][0] : [];
                $user_data = new UserData();
                $user_data->user_id        = isset($response['id'])            ? $response['id']            : 0;
                $user_data->user_firstname = isset($response['first_name'])    ? $response['first_name']    : 0;
                $user_data->user_lastname  = isset($response['last_name'])     ? $response['last_name']     : 0;
                $user_data->user_bdate     = isset($response['bdate'])         ? $response['bdate']         : 0;
                $user_data->user_city      = isset($response['city']['title']) ? $response['city']['title'] : 0;
                $user_data->user_photo     = isset($response['photo_max'])     ? $response['photo_max']     : 0;
                $user_data->user_phone     = isset($response['home_phone'])    ? $response['home_phone']    : 0;
                Yii::app()->session['user_data']   = $user_data->attributes;

                $this->redirect('/user/register');
            }
            else
            {
                Yii::app()->session['user_social'] = $social->attributes;

                $this->redirect('/user/register');
            }
        }
        else
        {
            $this->redirect('/user/register');
        }
    }
}