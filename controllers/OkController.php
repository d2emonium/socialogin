<?php

class OkController extends SocialController
{
    var $scheme          = "http://";
    var $social_url      = "odnoklassniki.ru";

    var $login_url       = 'oauth/authorize';
    var $login_subdomain = "www";

    public function getClient_id()
    {
        return $this->module->ok['client_id'];
    }

    public function getResponse_type()
    {
        return $this->module->ok['response_type'];
    }

    public function getClient_secret()
    {
        return $this->module->ok['client_secret'];
    }

    public function getGrant_type()
    {
        return $this->module->ok['grant_type'];
    }

    public function getPublic_key()
    {
        return $this->module->ok['public_key'];
    }

    public function actionLogin()
    {
        // Анализируем строку на наличие code.
        if(isset($_GET['code']) && $_GET['code'])
        {
            $code = $_GET['code'];

            $url = $this->generateUrl('oauth/token.do', [], "api");
            $data = $this->getFromUrl($url, "post", [
                "code"          => $code,
                "redirect_uri"  => $this->createAbsoluteUrl('ok/login'),
                "grant_type"    => $this->grant_type,
                "client_id"     => $this->client_id,
                "client_secret" => $this->client_secret,
            ]);
            $access_params = CJSON::decode($data);

            $access_token = isset($access_params['access_token']) ? $access_params['access_token'] : 0;
            $sign         = md5('application_key='.$this->public_key.'method=users.getCurrentUser'.
                md5($access_token.$this->client_secret));

            $url = $this->generateUrl('fb.do', [], "api");
            $data = $this->getFromUrl($url, "get", [
                "method"          => "users.getCurrentUser",
                "access_token"    => $access_token,
                "application_key" => $this->public_key,
                "sig"             => $sign,
            ]);
            $params = CJSON::decode($data);

            $user_id = isset($params['uid']) ? $params['uid'] : 0;

            $social = UserSocials::model()->findByAttributes(['user_social_ok' => $user_id]);
            if(!$social)
            {
                $social = new UserSocials();
                $social->user_social_ok = $user_id;
                Yii::app()->session['user_social'] = $social->attributes;

                $response = isset($params) ? $params : [];
                $user_data = new UserData();
                $user_data->user_id        = isset($response['uid'])              ? $response['uid']              : 0;
                $user_data->user_firstname = isset($response['first_name'])       ? $response['first_name']       : 0;
                $user_data->user_lastname  = isset($response['last_name'])        ? $response['last_name']        : 0;
                $user_data->user_bdate     = isset($response['birthday'])         ? $response['birthday']         : 0;
                $user_data->user_city      = isset($response['location']['city']) ? $response['location']['city'] : 0;
                $user_data->user_photo     = isset($response['pic_1'])            ? $response['pic_1']            : 0;
                Yii::app()->session['user_data']   = $user_data->attributes;

                $this->redirect('/user/register');
            }
            else
            {
                echo CJSON::encode($social->attributes);
                echo $data;
            }
        }
        else
        {
            //http://mysite.com/vklogin?error=invalid_request&error_description=Invalid+display+parameter
        }
    }
}