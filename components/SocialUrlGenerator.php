<?php
class SocialUrlGenerator extends CComponent {
    public function init()
    {
    }

    private function generateUrl($url, $params)
    {
        $param_txt = "";
        foreach($params as $k=>$v)
        {
            $param_txt .= ($param_txt ? '&' : '?').$k.'='.$v;
        }
        return $url.$param_txt;
    }

    public function getVk_url()
    {
        return $this->generateUrl("https://oauth.vk.com/authorize", [
            "client_id"     => Yii::app()->getModule('socialogin')->vk['client_id'],
            "redirect_uri"  => Yii::app()->getBaseUrl(true).Yii::app()->urlManager->createUrl('/socialogin/vk/login'),
            "response_type" => Yii::app()->getModule('socialogin')->vk['response_type'],
        ]);
    }

    public function getOk_url()
    {
        return $this->generateUrl("https://www.odnoklassniki.ru/oauth/authorize", [
            "client_id"     => Yii::app()->getModule('socialogin')->ok['client_id'],
            "redirect_uri"  => Yii::app()->getBaseUrl(true).Yii::app()->urlManager->createUrl('/socialogin/ok/login'),
            "response_type" => Yii::app()->getModule('socialogin')->ok['response_type'],
        ]);
    }

    public function getFb_url()
    {
        return $this->generateUrl("https://www.facebook.com/dialog/oauth", [
            "client_id"     => Yii::app()->getModule('socialogin')->fb['client_id'],
            "redirect_uri"  => Yii::app()->getBaseUrl(true).Yii::app()->urlManager->createUrl('/socialogin/fb/login'),
            "response_type" => Yii::app()->getModule('socialogin')->fb['response_type'],
        ]);
    }
} 