<?php

class SocialController extends Controller
{
    var $scheme          = "http://";
    var $social_url      = "odnoklassniki.ru";

    var $login_url       = 'oauth/authorize';
    var $login_subdomain = "www";

    var $auth_params = [];

    public function getClient_id()
    {
        return $this->module->social['client_id'];
    }

    public function getResponse_type()
    {
        return $this->module->social['response_type'];
    }

    public function actionIndex()
	{
        $this->redirect('url');
	}

    public function actionUrl()
    {
        echo $this->loginUrl();
    }

    public function loginUrl()
    {
        $login_params = [
            "client_id"     => $this->client_id,
            "redirect_uri"  => $this->createAbsoluteUrl('login'),
            "response_type" => $this->response_type,
        ];
        $login_params = array_merge($login_params, $this->auth_params);
        if($this->version){
            $login_params["v"] = $this->version;
        }

        return $this->generateUrl($this->login_url, $login_params, $this->login_subdomain);
    }

    protected function generateUrl($action="", $params=[], $subdomain="www")
    {
        $param_txt = "";
        foreach($params as $k=>$v)
        {
            $param_txt .= ($param_txt ? '&' : '?').$k.'='.$v;
        }
        return $this->scheme.$subdomain.".".$this->social_url."/".$action.$param_txt;
    }

    protected function getFromUrl($url, $method='get', $params=[])
    {
        $params = array_merge($params, $this->auth_params);

        $param_txt = "";
        foreach($params as $k=>$v)
        {
            $param_txt .= ($param_txt ? '&' : '').$k.'='.$v;
        }

        if(($method == 'get') && $param_txt)
        {
            $url .= '?'.$param_txt;
        }
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        if($method == 'post')
        {
            curl_setopt($ch,CURLOPT_POSTFIELDS,$param_txt);
        }
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}