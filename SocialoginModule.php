<?php

class SocialoginModule extends CWebModule
{
    var $vk = [
        'client_id'     => 0,
        'client_secret' => 0,
        'response_type' => 'code',
        'fields'        => 'uid,first_name,last_name,nickname,screen_name,sex,bdate,city,country,timezone,photo',
    ];
    var $ok = [
        'client_id'     => 0,
        'client_secret' => 0,
        'response_type' => 'code',

        'public_key'    => 0,
        'grant_type'    => 'authorization_code',
    ];
    var $fb = [
        'client_id'     => 0,
        'client_secret' => 0,
        'response_type' => 'code',
    ];

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'socialogin.models.*',
			'socialogin.components.*',
		));
	}

    public function getVk_url()
    {
        $g = new SocialUrlGenerator();
        return $g->vk_url;
    }

    public function getOk_url()
    {
        $g = new SocialUrlGenerator();
        return $g->ok_url;
    }

    public function getFb_url()
    {
        $g = new SocialUrlGenerator();
        return $g->fb_url;
    }

    public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
