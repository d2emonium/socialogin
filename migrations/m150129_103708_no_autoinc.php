<?php

class m150129_103708_no_autoinc extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('user_socials', 'user_id');
        $this->addColumn('user_socials', 'user_id', 'int');
        $this->addPrimaryKey('pk', 'user_socials', 'user_id');
	}

	public function down()
	{
        $this->dropColumn('user_socials', 'user_id');
        $this->addColumn('user_socials', 'user_id', 'pk');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}