<?php

class m150125_213708_user_socials_table extends CDbMigration
{
    public function up()
    {
        $this->createTable('user_socials',[
            'user_id'        => 'pk',
            'user_social_vk' => 'VARCHAR(32)',
            'user_social_ok' => 'VARCHAR(32)',
            'user_social_fb' => 'VARCHAR(32)',
        ]);
    }

    public function down()
    {
        $this->dropTable('user_socials');
        return true;
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}